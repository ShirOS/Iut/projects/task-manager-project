# Project Task Manager

## Binome

	- Hebert Florian
	- Caillot Alexandre

## Installation

	- git clone https://gitlab.com/ShirOS/Iut/projects/task-manager-project.git
	- cd task-manager-project
	- composer install
	- bin/console doctrine:fixtures:load
	- [ bin/console app:create:admin (Les fixtures créent déjà un admin lié à un projet) ]
	- bin/console server:run