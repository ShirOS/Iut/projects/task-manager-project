<?php

    namespace App\Controller\Back\User;

    use App\Controller\AbstractController;
    use App\Entity\User\User;
    use App\Form\Check\User\UserCheck;
    use App\Form\Type\User\UserType;
    use App\Service\User\UserService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class UserController
     * @package App\Controller\Back\User
     *
     * @Route("/backoffice/users")
     */
    class UserController extends AbstractController
    {
	    /**
	     * @var UserService
	     */
    	protected $userService;
    	
	    /**
	     * HomeController constructor.
	     */
	    public function __construct(UserService $userService)
	    {
	    	$this->userService = $userService;
	    }
	
	    /**
	     * @Route("/", name="bo_user_index")
	     *
	     * @param Request $request
	     *
	     * @return Response
	     */
	    public function index(Request $request): Response
	    {
	    	$users = $this->userService->findAll();
	    	
	    	$vars = compact('users');
	    	return $this->render('Back/User/index.html.twig', $vars);
	    }
	
	    /**
         * @Route("/add", name="bo_user_add")
         *
         * @param Request $request
         *
         * @return Response
         */
        public function add(Request $request): Response
        {
	        $form = $this->createForm(UserType::class);
	        $form->handleRequest($request);
	        $formView = $form->createView();
	
	        $errors = $this->getFormErrors($form);
	
	        if ($this->isPostRequest($request, $form)) {
		        try {
			        $datas = $form->getData();
			        $this->userService->create($datas);
			
			        return $this->redirectToRoute('bo_user_index');
		        } catch (\Throwable $throwable) {
			        $errors = $this->userService->getFormErrors();
			        if (!is_a($throwable, BadRequestHttpException::class)) {
				        array_push($errors, $throwable->getMessage());
			        }
		        }
	        }
	
	        $var = compact('formView', 'errors');
	        return $this->render('Back/User/edit.html.twig', $var);
        }
	
	    /**
	     * @Route("/edit/{id}", name="bo_user_edit")
	     *
	     * @param Request $request
	     * @param User $user
	     *
	     * @return Response
	     */
	    public function edit(Request $request, User $user): Response
	    {
		    $edit = true;
		    $userEdit = [];
		
		    $userEdit[UserCheck::PARAM_ROLE] = $user->getRole();
		
		    $form = $this->createForm(UserType::class, $userEdit, ['edit' => $edit]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->userService->edit($user, $datas);
				
				    return $this->redirectToRoute('bo_user_index');
			    } catch (\Throwable $throwable) {
				    $errors = $this->userService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('user', 'edit', 'formView', 'errors');
		    return $this->render('Back/User/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/delete/{id}", name="bo_user_delete")
	     *
	     * @param User $user
	     *
	     * @return Response
	     */
	    public function delete(User $user): Response
	    {
	    	$this->userService->delete($user);
		    return $this->redirectToRoute('bo_user_index');
	    }
    }
?>