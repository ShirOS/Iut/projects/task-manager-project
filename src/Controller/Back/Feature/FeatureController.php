<?php

    namespace App\Controller\Back\Feature;

    use App\Controller\AbstractController;
    use App\Entity\Feature\Feature;
    use App\Entity\Project\Project;
    use App\Form\Check\Feature\FeatureCheck;
    use App\Form\Type\Feature\FeatureType;
    use App\Service\Feature\FeatureService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class FeatureController
     * @package App\Controller\Back\Feature
     *
     * @Route("/backoffice")
     */
    class FeatureController extends AbstractController
    {
	    /**
	     * @var FeatureService
	     */
    	protected $featureService;
	
	    /**
	     * HomeController constructor.
	     *
	     * @param FeatureService $featureService
	     */
	    public function __construct(FeatureService $featureService)
	    {
	    	$this->featureService = $featureService;
	    }
	
	    /**
         * @Route("/project/{id}/features/add", name="bo_feature_add")
         *
         * @param Request $request
	     * @param Project $project
         *
         * @return Response
         */
        public function add(Request $request, Project $project): Response
        {
	        $form = $this->createForm(FeatureType::class);
	        $form->handleRequest($request);
	        $formView = $form->createView();
	
	        $errors = $this->getFormErrors($form);
	
	        if ($this->isPostRequest($request, $form)) {
		        try {
			        $datas = $form->getData();
			        $this->featureService->create($project, $datas);
			
			        return $this->redirectToRoute('bo_project_edit', ['id' => $project->getId()]);
		        } catch (\Throwable $throwable) {
			        $errors = $this->featureService->getFormErrors();
			        if (!is_a($throwable, BadRequestHttpException::class)) {
				        array_push($errors, $throwable->getMessage());
			        }
		        }
	        }
	
	        $var = compact('project', 'formView', 'errors');
	        return $this->render('Back/Feature/edit.html.twig', $var);
        }
	
	    /**
	     * @Route("/features/edit/{id}", name="bo_feature_edit")
	     *
	     * @param Request $request
	     * @param Feature $feature
	     *
	     * @return Response
	     */
	    public function edit(Request $request, Feature $feature): Response
	    {
	    	$project = $feature->getProject();
	    	
		    $edit = true;
		    $featureEdit = [];
		
		    $featureEdit[FeatureCheck::PARAM_TITLE] = $feature->getTitle();
		    $featureEdit[FeatureCheck::PARAM_PRICE] = $feature->getPrice();
	    	
		    $form = $this->createForm(FeatureType::class, $featureEdit, ['edit' => $edit]);
		    $form->handleRequest($request);
		    $formView = $form->createView();
		
		    $errors = $this->getFormErrors($form);
		
		    if ($this->isPostRequest($request, $form)) {
			    try {
				    $datas = $form->getData();
				    $this->featureService->edit($feature, $datas);
				
				    return $this->redirectToRoute('bo_project_edit', ['id' => $project->getId()]);
			    } catch (\Throwable $throwable) {
				    $errors = $this->featureService->getFormErrors();
				    if (!is_a($throwable, BadRequestHttpException::class)) {
					    array_push($errors, $throwable->getMessage());
				    }
			    }
		    }
		
		    $var = compact('project', 'feature', 'edit', 'formView', 'errors');
		    return $this->render('Back/Feature/edit.html.twig', $var);
	    }
	
	    /**
	     * @Route("/features/delete/{id}", name="bo_feature_delete")
	     *
	     * @param Request $request
	     * @param Feature $feature
	     *
	     * @return Response
	     */
	    public function delete(Request $request, Feature $feature): Response
	    {
		    $project = $feature->getProject();
		    
	    	$this->featureService->delete($feature);
		    return $this->redirectToRoute('bo_project_edit', ['id' => $project->getId()]);
	    }
    }
?>