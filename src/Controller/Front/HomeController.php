<?php

    namespace App\Controller\Front;

    use App\Controller\AbstractController;
    use App\Entity\User\User;
    use App\Service\Project\ProjectService;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;

    /**
     * Class HomeController
     * @package App\Controller\Front
     */
    class HomeController extends AbstractController
    {
        protected $projectService;

        /**
         * HomeController constructor.
         */
        public function __construct(ProjectService $projectService)
        {
            $this->projectService = $projectService;
        }


        /**
         * @Route("/", name="app_index")
         *
         * @param Request $request
         *
         * @return Response
         */
        public function index(Request $request): Response
        {
        	/** @var User $user */
        	$user = $this->getUser();
	        $project = $user->getProject();

            $jsonTasks = $this->projectService->getJsonTask($project);

	        $vars = compact('project', 'jsonTasks');
	        return $this->render('Front/index.html.twig', $vars);
        }
    }
?>