<?php

    namespace App\Form\Check\Project;

    use App\Form\Check\AbstractCheck;

    class ProjectManageUserCheck extends AbstractCheck
    {
        public const PARAM_USERS = 'users';
    }
?>