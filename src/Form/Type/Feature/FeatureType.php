<?php

    namespace App\Form\Type\Feature;

    use App\Form\Check\Feature\FeatureCheck;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\MoneyType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class FeatureType extends AbstractType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $edit = $options['edit'];
            $buttonId = 'edit';
            $buttonName = 'Valider';

            if (!$edit) {
                $buttonId = 'create';
                $buttonName = 'Créer';
            }
	
	        $builder
		        ->add(FeatureCheck::PARAM_TITLE,
			        TextType::class,
			        [
				        'label'      => 'Titre :',
                        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                        'attr'       => ['class' => 'form-control'],
				        'required'   => true
			        ])
		        ->add(FeatureCheck::PARAM_PRICE,
			        MoneyType::class,
			        [
				        'label'    => 'Prix :',
                        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                        'attr'       => ['class' => 'form-control'],
				        'required' => true
			        ])
	        ;

            $builder->add($buttonId,
                SubmitType::class,
                [
                    'label'      => $buttonName,
                    'attr'       => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
                ]);
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'edit'       => false
            ]);
        }
    }
?>