<?php

    namespace App\Form\Type\User;

    use App\Entity\User\Role;
    use App\Form\Check\User\UserCheck;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\PasswordType;
    use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class UserType extends AbstractType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $edit = $options['edit'];
            $buttonId = 'edit';
            $buttonName = 'Valider';

            if (!$edit) {
                $buttonId = 'create';
                $buttonName = 'Créer';

                $builder
                    ->add(UserCheck::PARAM_USERNAME,
                        TextType::class,
                        [
                            'label'      => 'Username :',
                            'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                            'attr'       => ['class' => 'form-control'],
                            'required'   => true
                        ])
                    ->add(UserCheck::PARAM_PASSWORD,
                        RepeatedType::class,
                        [
                            'type'            => PasswordType::class,
                            'first_options'   => [
                                'label'      => 'Mot de Passe :',
                                'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                                'attr'       => ['class' => 'form-control'],
                                'required'   => true
                            ],
                            'second_options'  => [
                                'label'      => 'Confirmation du Mot de Passe :',
                                'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                                'attr'       => ['class' => 'form-control'],
                                'required'   => true
                            ],
                            'invalid_message' => 'Les mots de passe doivent correspondre.'
                        ])
                ;
            }

            $builder
                ->add(UserCheck::PARAM_ROLE,
                    ChoiceType::class,
                    [
                        'choices'    => array_flip(Role::ROLES),
                        'label'      => 'Rôle :',
                        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
                        'attr'       => ['class' => 'custom-select custom-select-lg mb-3'],
                        'multiple'   => false,
                        'expanded'   => false,
                        'required'   => false,
                    ])
            ;

            $builder->add($buttonId,
                SubmitType::class,
                [
                    'label'      => $buttonName,
                    'attr'       => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
                ]);
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'edit'       => false
            ]);
        }
    }
?>