<?php

    namespace App\Form\Type\Tag;

    use App\Form\Check\Tag\TagCheck;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ColorType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class TagType extends AbstractType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $edit = $options['edit'];
            $buttonId = 'edit';
            $buttonName = 'Valider';
            
            if (!$edit) {
                $buttonId = 'create';
                $buttonName = 'Créer';
            }
	
	        $builder
		        ->add(TagCheck::PARAM_TYPE,
			        TextType::class,
			        [
				        'label'      => 'Type :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'required'   => true
			        ])
		        ->add(TagCheck::PARAM_COLOR,
			        ColorType::class,
			        [
				        'label'      => 'Couleur :',
				        'label_attr' => ['class' => 'mtb-5 text-blue text-italic text-bold'],
				        'attr'       => ['class' => 'form-control'],
				        'required'   => true
			        ])
		        ->add($buttonId,
		            SubmitType::class,
		            [
		                'label'      => $buttonName,
		                'attr'       => ['class' => 'btn btn-primary d-block d-center w-auto mtb-10']
		            ])
            ;
        }

        public function configureOptions(OptionsResolver $resolver)
        {
            $resolver->setDefaults([
                'data_class' => NULL,
                'edit'       => false
            ]);
        }
    }
?>