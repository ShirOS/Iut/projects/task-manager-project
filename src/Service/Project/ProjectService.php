<?php

    namespace App\Service\Project;

    use App\Entity\Feature\Feature;
    use App\Entity\Project\Project;
    use App\Entity\Task\Task;
    use App\Entity\User\User;
    use App\Form\Check\Project\ProjectCheck;
    use App\Form\Check\Project\ProjectManageUserCheck;
    use App\Repository\Project\ProjectRepository;
    use App\Service\Exception\ExceptionHandlerService;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

    /**
     * Class ProjectService
     * @package App\Service\Project
     */
    class ProjectService
    {
        /** @var EntityManagerInterface */
        protected $em;

        /** @var ProjectRepository */
        protected $repository;
	
	    /** @var ProjectCheck */
	    protected $projectCheck;
	
	    /** @var ProjectManageUserCheck */
	    protected $projectManageUserCheck;

        /** @var ExceptionHandlerService */
        protected $exceptionHandlerService;
	
	    /**
	     * UserService constructor.
	     * @param EntityManagerInterface $em
	     * @param ProjectCheck $check
	     * @param ExceptionHandlerService $exceptionHandlerService
	     */
        public function __construct(EntityManagerInterface $em, ProjectCheck $projectCheck, ProjectManageUserCheck $projectManageUserCheck, ExceptionHandlerService $exceptionHandlerService)
        {
            $this->em = $em;
            $this->repository = $em->getRepository(Project::class);
	
	        $this->projectCheck = $projectCheck;
	        $this->projectManageUserCheck = $projectManageUserCheck;
            $this->exceptionHandlerService = $exceptionHandlerService;
        }

        /**
         * @param Project $project
         * @return Project
         */
        protected function persist(Project $project): Project
        {
            try {
                $this->em->persist($project);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }

            return $project;
        }

        /**
         * @return array
         */
        public function getProjectFormErrors(): array
        {
            return $this->projectCheck->getErrors();
        }
	
	    /**
	     * @return array
	     */
	    public function getManageUserFormErrors(): array
	    {
		    return $this->projectManageUserCheck->getErrors();
	    }

        /**
         * @return string
         */
        public function getJsonTask(Project $project): string
        {
            $jsonTasks = [];

            $features = $project->getFeatures();
            /** @var Feature $feature */
            foreach ($features as $feature) {
                $tasks = $feature->getTasks();

                /** @var Task $task */
                foreach ($tasks as $task) {
                    $start = new \DateTime();
                    $start->setTimestamp($task->getStartDate());
                    $end = new \DateTime();
                    $end->setTimestamp($task->getEndDate());

                    $jsonTask = [
                        'title' => $task->getTitle(),
                        'start' => $start->format('Y-m-d\Th:i:s'),
                        'end' => $end->format('Y-m-d\Th:i:s'),
                    ];

                    array_push($jsonTasks, $jsonTask);
                }
            }

            return json_encode($jsonTasks);
        }
	
	
	    # -------------------------------------------------------------
	    #   Operation CRUD
	    # -------------------------------------------------------------
		
	    /**
	     * @return array
	     */
	    public function findAll(): array
	    {
	    	return $this->repository->findAll();
	    }
	    

        /**
         * @param array $datas
         *
         * @return Project
         */
        public function create(array $datas): Project
        {
            $this->projectCheck->create($datas);

            if (!$this->projectCheck->isValid()) {
                throw new BadRequestHttpException('Some parameters are missing');
            }

            $project = new Project();
	        $project
                ->setCode($datas[ProjectCheck::PARAM_CODE])
	            ->setRealizationPeriod($datas[ProjectCheck::PARAM_REALIZATION_PERIOD])
	            ->setState($datas[ProjectCheck::PARAM_STATE])
            ;

            return $this->persist($project);
        }
	
	    /**
	     * @param Project $project
	     * @param array $datas
	     *
	     * @return Project
	     */
	    public function edit(Project $project, array $datas): Project
	    {
		    if (array_key_exists(ProjectCheck::PARAM_STATE, $datas)) {
			    if (!is_null($datas[ProjectCheck::PARAM_STATE])) {
				    $project
					    ->setState($datas[ProjectCheck::PARAM_STATE])
				    ;
			    }
		    }
		
		    try {
			    $this->em->flush();
		    } catch (\Throwable $throwable) {
			    $this->exceptionHandlerService->treatment($throwable);
		    }
		
		    return $project;
	    }
	
	    /**
	     * @param Project $project
	     * @param array $datas
	     *
	     * @return Project
	     */
	    public function manageUser(Project $project, array $datas): Project
	    {
		    if (array_key_exists(ProjectManageUserCheck::PARAM_USERS, $datas)) {
		    	/** @var User $user */
			    foreach ($project->getUsers() as $user) {
			    	$user->setProject(NULL);
			    }
			    
			    $project->getUsers()->clear();
			    $users = $datas[ProjectManageUserCheck::PARAM_USERS];
			    
			    /** @var User $user */
			    foreach ($users as $user) {
			    	$project->addUser($user);
			    }
		    }
		    
		    try {
			    $this->em->flush();
		    } catch (\Throwable $throwable) {
			    $this->exceptionHandlerService->treatment($throwable);
		    }
		
		    return $project;
	    }


        /**
         * @param Project $project
         */
        public function delete(Project $project)
        {
	        /** @var User $user */
	        foreach ($project->getUsers() as $user) {
		        $user->setProject(NULL);
	        }
        	
            try {
                $this->em->remove($project);
                $this->em->flush();
            } catch (\Throwable $throwable) {
                $this->exceptionHandlerService->treatment($throwable);
            }
        }
    }

?>