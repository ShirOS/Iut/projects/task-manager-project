<?php

    namespace App\Entity\Project;

    use App\Entity\Feature\Feature;
    use App\Entity\User\User;
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Collections\Collection;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="App\Repository\Project\ProjectRepository")
     */
    class Project
    {
        /**
         * @ORM\Id()
         * @ORM\GeneratedValue()
         * @ORM\Column(type="integer")
         */
        protected $id;

        /**
         * @ORM\Column(type="string", length=255)
         */
        protected $code;
	
	    /**
	     * @var string
	     * @ORM\Column(type="string", length=255)
	     */
	    protected $state;
	
	    /** @var int */
	    protected $duration;


        # -------------------------------------------------------------
        #   Association
        # -------------------------------------------------------------

        /**
         * @var Collection
         * @ORM\OneToMany(targetEntity="App\Entity\Feature\Feature", mappedBy="project", cascade={"persist", "remove"})
         */
        protected $features;

        /**
         * @var Collection
         * @ORM\OneToMany(targetEntity="App\Entity\User\User", mappedBy="project", cascade={"persist"})
         */
        protected $users;
	
	    /**
	     * Project constructor.
	     */
	    public function __construct()
	    {
	    	$this->features = new ArrayCollection();
		    $this->users = new ArrayCollection();
		    
		    $this->duration = 0;
	    }
	
	
	
	    # -------------------------------------------------------------
        #   Id
        # -------------------------------------------------------------

        /**
         * @return mixed
         */
        public function getId()
        {
            return $this->id;
        }


        # -------------------------------------------------------------
        #   Code
        # -------------------------------------------------------------

        /**
         * @return null|string
         */
        public function getCode(): ?string
        {
            return $this->code;
        }

        /**
         * @param string $code
         * @return Project
         */
        public function setCode(string $code): self
        {
            $this->code = $code;

            return $this;
        }
	
	
	    # -------------------------------------------------------------
	    #   State
	    # -------------------------------------------------------------
	
	    /**
	     * @return string
	     */
	    public function getState(): string
	    {
		    return $this->state;
	    }
	
	    /**
	     * @param string $state
	     *
	     * @return self
	     */
	    public function setState(string $state): self
	    {
		    $this->state = $state;
		    return $this;
	    }
	
	
	    # -------------------------------------------------------------
	    #   Duration
	    # -------------------------------------------------------------
	
	    /**
	     * @return int
	     */
	    public function getDuration(): int
	    {
		    return is_null($this->duration) ? 0 : $this->duration;
	    }
	
	    /**
	     * @param int $duration
	     *
	     * @return self
	     */
	    public function setDuration(int $duration): self
	    {
		    $this->duration = $duration;
		    return $this;
	    }


        # -------------------------------------------------------------
        #   Features
        # -------------------------------------------------------------

        /**
         * @return Collection
         */
        public function getFeatures()
        {
            return $this->features;
        }

        /**
         * @param Feature $feature
         * @return self
         */
        public function addFeature(Feature $feature): self
        {
	        $feature->setProject($this);
            $this->features->add($feature);

            return $this;
        }

        /**
         * @param Collection $features
         * @return self
         */
        public function setFeatures($features): self
        {
            $this->features = $features;

            return $this;
        }


        # -------------------------------------------------------------
        #   Users
        # -------------------------------------------------------------

        /**
         * @return Collection
         */
        public function getUsers()
        {
            return $this->users;
        }

        /**
         * @param User $user
         * @return self
         */
        public function addUser(User $user): self
        {
	        $user->setProject($this);
            $this->users->add($user);

            return $this;
        }

        /**
         * @param Collection $users
         * @return self
         */
        public function setUsers($users): self
        {
            $this->users = $users;

            return $this;
        }
    }
?>