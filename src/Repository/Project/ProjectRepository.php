<?php

namespace App\Repository\Project;

use App\Entity\Feature\Feature;
use App\Entity\Project\Project;
use App\Repository\Feature\FeatureRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ProjectRepository extends ServiceEntityRepository
{
	/** @var FeatureRepository */
	protected $featureRepository;
	
    public function __construct(RegistryInterface $registry, FeatureRepository $featureRepository)
    {
        parent::__construct($registry, Project::class);
        
        $this->featureRepository = $featureRepository;
    }
	
	/**
	 * @param mixed $id
	 * @param null $lockMode
	 * @param null $lockVersion
	 *
	 * @return Project|null|object
	 */
	public function find($id, $lockMode = NULL, $lockVersion = NULL)
	{
		/** @var Project $project */
		$project = parent::find($id, $lockMode, $lockVersion);
		
		if (!is_null($project)) {
			$this->prepareProject($project);
		}
		
		return $project;
	}
	
	/**
	 * @param array $criteria
	 * @param array|NULL $orderBy
	 *
	 * @return Project|null|object
	 */
	public function findOneBy(array $criteria, array $orderBy = null)
	{
		/** @var Project $project */
		$project = parent::findOneBy($criteria, $orderBy);
		
		if (!is_null($project)) {
			$this->prepareProject($project);
		}
		
		return $project;
	}
	
	/**
	 * @return array
	 */
	public function findAll()
	{
		$projects = parent::findAll();
		
		/** @var Project $project */
		foreach ($projects as $project) {
			$this->prepareProject($project);
		}
		
		return $projects;
	}
	
	/**
	 * @param array $criteria
	 * @param array|NULL $orderBy
	 * @param null $limit
	 * @param null $offset
	 *
	 * @return array
	 */
	public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
	{
		$projects = parent::findBy($criteria, $orderBy, $limit, $offset);
		
		/** @var Project $project */
		foreach ($projects as $project) {
			$this->prepareProject($project);
		}
		
		return $projects;
	}
	
	/**
	 * Prepare some calculated vars for project (Example : Duration)
	 *
	 * @param Project $project
	 */
	public function prepareProject(Project $project)
	{
        $features = $project->getFeatures();
		
        /** @var Feature $feature */
		foreach ($features as $feature) {
            $this->featureRepository->prepareFeature($feature);
            
            $projectDuration = $project->getDuration();
            $featureDuration = $feature->getDuration();
            
            $project->setDuration($projectDuration + $featureDuration);
        }
	}
}
